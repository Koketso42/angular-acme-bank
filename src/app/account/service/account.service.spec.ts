import { inject, TestBed, fakeAsync, tick, async } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ApiService } from '../../shared/service/api.service';
import { AccountService } from './account.service';
import { of } from 'rxjs';
import { AccountType, ClientBankProfile, BankAccount } from '../model/account.model';

describe('AccountService', () => {
    let mockAccounts;
    let apiService;
    let mockProfile;

    beforeEach(() => {
        mockAccounts = [
            {
                account_number: '12345',
                account_type: AccountType.CHEQUE,
                balance: '300'
            },
            {
                account_number: '6789',
                account_type: AccountType.SAVINGS,
                balance: '800'
            }
        ];

        apiService = {
            accounts: {
                get: jasmine.createSpy('get').and.returnValue(of(mockAccounts))
            }
        };

        mockProfile = new ClientBankProfile();
        mockProfile.availableAccounts = [
            new BankAccount(mockAccounts[0]),
            new BankAccount(mockAccounts[1])
        ];
        mockProfile['_overallBalance'] = 1100;

        TestBed.configureTestingModule({
            imports: [
                HttpClientModule
            ],
            providers: [
                HttpClient,
                AccountService,
                { provide: ApiService, useValue: apiService }
            ]
        });
    });

    /**
     * Honestly, in my opinion there's no need to check if this service was successfully created
     * as I assume that it should be defined then test it's functionality
     * (I think it doesn't even, up the test coverage, functions, and branches)
     */
    it('should be created', inject([AccountService], (service: AccountService) => {
        expect(service).toBeTruthy();
    }));

    it('should call accounts api', inject([AccountService, ApiService],
        (service: AccountService) => {
            expect(service).toBeTruthy();
            service.getBankAccounts().subscribe((response) => {
                expect(apiService.accounts.get).toHaveBeenCalled();
                expect(response).toBe(mockAccounts);
            });
        }));

    it('should get client bank profile',
        async(inject([AccountService], (service: AccountService) => {
            service.getClientBankProfile().then(profile => {
                expect(apiService.accounts.get).toHaveBeenCalled();
                expect(profile).toEqual(mockProfile);
            });
        }))
    );
});
