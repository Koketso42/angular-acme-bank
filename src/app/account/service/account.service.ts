import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/service/api.service';
import { IAccount, BankAccount, ClientBankProfile } from '../model/account.model';
import { Observable } from 'rxjs';

@Injectable()
export class AccountService {

    constructor(private _apiService: ApiService) { }

    getBankAccounts(): Observable<any> {
        return this._apiService.accounts.get();
    }

    getClientBankProfile(): Promise<ClientBankProfile> {
        return new Promise(async (resolve, reject) => {
            await this.getBankAccounts().subscribe(accounts => {
                resolve(this.setupClientBankProfile(accounts as Array<IAccount>));
            }, (error) => {
                reject(error);
            });
        });
    }

    private async setupClientBankProfile(accounts: Array<IAccount>): Promise<ClientBankProfile> {
        let overallBalance = 0;
        const clientBankProfile: ClientBankProfile = new ClientBankProfile();
        clientBankProfile.availableAccounts = await accounts.map((accountSchema: IAccount) => {
            const validBankAccount: BankAccount = new BankAccount(accountSchema);
            overallBalance += validBankAccount.balance;
            return validBankAccount;
        });

        clientBankProfile.balance = overallBalance;

        return clientBankProfile;
    }
}
