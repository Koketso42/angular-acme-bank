import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, ModuleWithProviders } from '@angular/core';
import { AccountService } from './service/account.service';
import { SharedModule } from '../shared/shared.module';
import { AccountListComponent } from './components/account-list/account-list.component';
import { WithdrawCashDialogComponent } from './components/withdraw-cash-dialog/withdraw-cash-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule.forRoot(),
    MatDialogModule,
    MatInputModule
  ],
  declarations: [
    AccountListComponent,
    WithdrawCashDialogComponent
  ],
  exports: [
    AccountListComponent,
    WithdrawCashDialogComponent
  ],
  providers: [
    AccountService
  ],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: [
    AccountListComponent,
    WithdrawCashDialogComponent
  ]
})
export class AccountModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AccountModule,
      providers: [
        AccountService
      ]
    };
  }
}
