/**
 * Interface shema for API response data (DTO)
 */
export interface IAccount {
    'account_number': string;
    'account_type': AccountType;
    'balance': string;
}

/**
 * Valid bank account types mapper
 */
export enum AccountType {
    CHEQUE = 'cheque',
    SAVINGS = 'savings'
}

/**
 * A valid bank account with all possible procedures a valid bank account can perform
 */
export class BankAccount {
    private static DEFAULT_MAX_OVERDRAFT = 500; // as per business rules

    accountNumber: number;
    accountType: AccountType;
    balance: number;
    overdraft: number;

    constructor(private _newAccount: IAccount = null) {
        this.setupNewAccount(this._newAccount);
    }

    /**
     * setup a valid bank account underwise create a new account (N.B: implementation might deffer depending on business cases)
     * @param newAccount -> IAccount
     */
    private setupNewAccount(newAccount: IAccount = null): void {
        this.accountNumber = Number(newAccount['account_number']) || this.generateAccountNumber();
        this.accountType = newAccount['account_type'] || AccountType.SAVINGS;
        this.balance = Number(newAccount['balance']) || 0;
        this.overdraft = this.assignDefaultOverdraft(newAccount['account_type'] as AccountType);
    }

    private assignDefaultOverdraft(accountType: AccountType): number {
        return accountType === AccountType.CHEQUE ? BankAccount.DEFAULT_MAX_OVERDRAFT : 0;
    }

    private generateAccountNumber(): number {
        return Math.floor((Math.random() * 10000000000) + 1); // dummy account number for testing
    }

    get availabe(): number {
        return this.balance + this.overdraft;
    }

    get canWithdraw(): boolean {
        return this.availabe > 0;
    }

    withdrawCash(amount): boolean {
        if (amount <= this.availabe) {
            this.balance -= amount; // can be negative depending on account type
            return true;
        }

        return false;
    }
}

/**
 * Most of the logic here, might move it to account manager service
 */
export class ClientBankProfile {
    availableAccounts: Array<BankAccount> = [];

    private _overallBalance: number;
    get balance(): number {
        return this._overallBalance;
    }
    set balance(totalAmount: number) {
        this._overallBalance = totalAmount;
    }

    constructor() { }

    private handleWithdrawal(withDrawalbankAccount: BankAccount, withdrawalAmount: number): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            switch (withDrawalbankAccount.accountType) {
                case AccountType.CHEQUE: {
                    // direct withdrawal because can have an overdraft
                    resolve(withDrawalbankAccount.withdrawCash(withdrawalAmount));
                    break;
                }
                case AccountType.SAVINGS: {
                    if (withDrawalbankAccount.balance >= withdrawalAmount) {
                        resolve(withDrawalbankAccount.withdrawCash(withdrawalAmount));
                    } else {
                        resolve(false); // no need to further withdraw in this case
                    }
                    break;
                }
            }
        });
    }

    private async updateAccountsOverallBalance(accounts: Array<BankAccount>, updateAccount: BankAccount): Promise<void> {
        let overallBalance = 0;
        this.availableAccounts = await accounts.map((bankAccount: BankAccount) => {
            if (bankAccount.accountNumber === updateAccount.accountNumber) {
                bankAccount.balance = updateAccount.balance;
            }
            overallBalance += bankAccount.balance;
            return bankAccount;
        });
        this.balance = overallBalance;
        return;
    }

    withdrawCashFromAccount(withDrawalbankAccount: BankAccount, withdrawalAmount: number): Promise<string> {
        return new Promise(async (resolve, reject) => {
            if (!withDrawalbankAccount.canWithdraw) {
                reject('Insuffient funds!');
            } else {
                const withdrawalAuthorized: boolean = await this.handleWithdrawal(withDrawalbankAccount, withdrawalAmount);

                if (withdrawalAuthorized) {
                    await this.updateAccountsOverallBalance(this.availableAccounts, withDrawalbankAccount);
                    resolve('Success'); // message as per business case
                } else {
                    reject('Insuffient funds!');
                }
            }
        });
    }
}
