import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BankAccount } from '../../model/account.model';

@Component({
    selector: 'app-withdraw-cash-dialog',
    templateUrl: 'withdraw-cash-dialog.component.html',
})
export class WithdrawCashDialogComponent {

    constructor(
        public dialogRef: MatDialogRef<WithdrawCashDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {
            bankAccount: BankAccount
            withdrawalAmount: number
        }
    ) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
