import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { AccountListComponent } from './account-list.component';
import { AccountService } from '../../service/account.service';
import { MatDialog, MatDialogModule, MatInputModule } from '@angular/material';
import { WithdrawCashDialogComponent } from '../withdraw-cash-dialog/withdraw-cash-dialog.component';
import { ClientBankProfile, BankAccount, AccountType } from '../../model/account.model';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../../../shared/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';

describe('AccountListComponent', () => {
    let component;
    let fixture;
    let accountService;
    let mockAccounts;
    let mockProfile;
    let dialog;

    beforeEach(async(() => {
        mockAccounts = [
            {
                account_number: '12345',
                account_type: AccountType.CHEQUE,
                balance: '300'
            },
            {
                account_number: '6789',
                account_type: AccountType.SAVINGS,
                balance: '800'
            }
        ];

        mockProfile = new ClientBankProfile();
        mockProfile.availableAccounts = [
            new BankAccount(mockAccounts[0]),
            new BankAccount(mockAccounts[1])
        ];
        mockProfile['_overallBalance'] = 1100;

        accountService = jasmine.createSpyObj('accountService', ['getClientBankProfile']);
        accountService.getClientBankProfile.and.returnValue(Promise.resolve(mockProfile));

        dialog = jasmine.createSpyObj('dialog', ['open']);
        dialog.open.and.returnValue({
            afterClosed: () => of(null)
        });

        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule.forRoot(),
                MatDialogModule,
                MatInputModule,
                BrowserAnimationsModule
            ],
            declarations: [
                AccountListComponent,
                WithdrawCashDialogComponent
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: AccountService, useValue: accountService },
                { provide: MatDialog, useValue: dialog }
            ]
        });

        fixture = TestBed.createComponent(AccountListComponent);
        component = fixture.debugElement.componentInstance;
    }));

    it('should get client bank profile when the component loads', fakeAsync(() => {
        component.ngOnInit();
        tick();
        expect(accountService.getClientBankProfile).toHaveBeenCalled();
        expect(component.clientPortfolio).toEqual(mockProfile);
    }));

    it('should fail to get client bank profile when account service api is down', fakeAsync(() => {
        accountService.getClientBankProfile.and.returnValue(Promise.reject());
        component.ngOnInit();
        tick();
        expect(accountService.getClientBankProfile).toHaveBeenCalled();
        expect(component.clientPortfolio).toBe(null);
    }));

    it('should promt an input dialog to withdraw cash', fakeAsync(() => {
        component.ngOnInit();
        tick();
        component.promptForWithdrawCashDialog(new BankAccount(mockAccounts[0]));
        tick();
        expect(dialog.open).toHaveBeenCalledWith(WithdrawCashDialogComponent, {
            width: 'auto',
            height: '200px',
            data: {
                bankAccount: new BankAccount(mockAccounts[0]),
                withdrawalAmount: component.withdrawalAmount
            }
        });
    }));

    it('should promt an input dialog to withdraw cash', fakeAsync(() => {
        dialog.open.and.returnValue({
            afterClosed: () => of(100)
        });
        spyOn(component, 'withdrawCash');
        component.withdrawalAmount = 100;
        const withdrawalBankAccount = new BankAccount(mockAccounts[0]);
        component.ngOnInit();
        tick();
        component.promptForWithdrawCashDialog(withdrawalBankAccount);
        tick();
        expect(dialog.open).toHaveBeenCalledWith(WithdrawCashDialogComponent, {
            width: 'auto',
            height: '200px',
            data: {
                bankAccount: withdrawalBankAccount,
                withdrawalAmount: component.withdrawalAmount
            }
        });
        expect(component['withdrawCash']).toHaveBeenCalledWith(withdrawalBankAccount, 100);
    }));
});
