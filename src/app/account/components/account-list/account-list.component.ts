import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../service/account.service';
import { ClientBankProfile, BankAccount } from '../../model/account.model';
import { MatDialog } from '@angular/material/dialog';
import { WithdrawCashDialogComponent } from '../withdraw-cash-dialog/withdraw-cash-dialog.component';

@Component({
    selector: 'app-account-list',
    templateUrl: './account-list.component.html'
})
export class AccountListComponent implements OnInit {
    clientPortfolio: ClientBankProfile = null;
    withdrawalAmount = 0;

    constructor(private accountService: AccountService, private dialog: MatDialog) { }

    private async withdrawCash(bankAccount: BankAccount, withdrawalAmount: number) {
        if (bankAccount.canWithdraw) {
            alert(await this.clientPortfolio.withdrawCashFromAccount(bankAccount, withdrawalAmount));
        }
    }

    ngOnInit(): void {
        // config loading state when the service is still loading
        this.accountService.getClientBankProfile().then((clientPortfolio: ClientBankProfile) => {
            console.log('clientPortfolio: ', clientPortfolio);
            this.clientPortfolio = clientPortfolio;
        }, (error) => {
            // configure error handler method
            console.error(error);
        });
    }

    promptForWithdrawCashDialog(withdrawalBankAccount: BankAccount) {
        const dialogRef = this.dialog.open(WithdrawCashDialogComponent, {
            width: 'auto',
            height: '200px',
            data: {
                bankAccount: withdrawalBankAccount,
                withdrawalAmount: this.withdrawalAmount
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.withdrawalAmount = result;
                this.withdrawCash(withdrawalBankAccount, this.withdrawalAmount);
            }
        });
    }
}
