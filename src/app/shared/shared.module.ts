import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { ApiService } from './service/api.service';
import { HttpClientModule } from '@angular/common/http';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MatToolbarModule } from '@angular/material';
@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        MatToolbarModule
    ],
    declarations: [
        NavBarComponent
    ],
    exports: [
        NavBarComponent
    ],
    providers: [
        ApiService
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                ApiService
            ]
        };
    }
}
