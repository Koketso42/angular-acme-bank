import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed, inject } from '@angular/core/testing';
import { ApiService } from './api.service';

/**
 * Might also test the service timeout
 */
describe('ApiService', () => {
   beforeEach(() => {
      TestBed.configureTestingModule({
         imports: [HttpClientModule],
         providers: [ApiService, HttpClient]
      });
   });

   it('should be created', inject([ApiService], (service: ApiService) => {
      expect(service).toBeTruthy();
   }));

   it('should have get methods', inject([ApiService], (service: ApiService) => {
      expect(service.accounts.get).toBeDefined();
   }));
});
