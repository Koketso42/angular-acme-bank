import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { timeout, catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

/**
 * More can be done here, createHeaders handler (append token for security reasons), put, post, delete
 * Interceptors might be needed especially for authentication...
 */
export class ApiSerive<T> {

    private _defaultTimeout = 25000;

    constructor(
        protected _http: HttpClient,
        protected _actionUrl: string,
        protected _TModel?: new (...args) => T
    ) {
    }

    get<Any>(timeOut: number = this._defaultTimeout): Observable<Any> {
        // checkHeaders and append http options (with auth access token)
        return this._http.get<any>(environment.baseUrl + '/' + this._actionUrl)
            .pipe(
                map(response => {
                    if (this._TModel) {
                        const objTModel = new this._TModel();
                        response = this.fromJson(objTModel, response);
                        return response;
                    }

                    return response;
                }),
                timeout(timeOut),
                catchError(error =>
                    throwError(error)
                )
            );
    }

    private fromJson(toObject, json: any): T {
        return Object.assign(toObject, json);
     }
}
