import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { ApiSerive } from './api';
import { environment } from '../../../environments/environment';

const urlEndPoint = 'testEndpoint';
const mockTestData: ITest = {
   id: 1,
   accountNumber: '12345'
};

interface ITest {
   id: number;
   accountNumber: string;
}

class Test implements ITest {
   constructor(public id: number, public accountNumber: string) { }
}

function getMethodTest(api, httpMock, mockData) {
   api.get()
      .subscribe((response) => {
         expect(response.id).toEqual(mockData.id);
      });
}

describe('Api', () => {
   beforeEach(() => {
      TestBed.configureTestingModule({
         imports: [HttpClientTestingModule],
         providers: [HttpClient]
      });
   });

   describe('get request', () => {
      it('should get the response with model',
         inject([HttpTestingController, HttpClient], (httpMock: HttpTestingController, http: HttpClient) => {
            const api: ApiSerive<ITest> = new ApiSerive<ITest>(http, urlEndPoint, Test);

            getMethodTest(api, httpMock, mockTestData);

            const req = httpMock.expectOne(`${environment.baseUrl}/${urlEndPoint}`);
            expect(req.request.method).toEqual('GET');
            req.flush(mockTestData);
         }));

      it('should get the response without model',
         inject([HttpTestingController, HttpClient], (httpMock: HttpTestingController, http: HttpClient) => {
            const api: ApiSerive<ITest> = new ApiSerive<ITest>(http, urlEndPoint);

            getMethodTest(api, httpMock, mockTestData);

            const req = httpMock.expectOne(`${environment.baseUrl}/${urlEndPoint}`);
            expect(req.request.method).toEqual('GET');
            req.flush(mockTestData);
         }));
   });

   afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
      httpMock.verify();
   }));
});
