import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiSerive } from './api';
import { IAccount } from '../../account/model/account.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiService {

    constructor(private _http: HttpClient) { }

    get accounts() {
        return new ApiSerive<IAccount>(this._http, environment.endPoints.accounts);
    }
}
