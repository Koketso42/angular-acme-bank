import { Component } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  template: `
    <mat-toolbar color="primary">
        <span>ACME Bank</span>
    </mat-toolbar>
  `
})
export class NavBarComponent {
}
