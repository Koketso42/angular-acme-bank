// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

let argv = require('minimist')(JSON.parse(process.env.npm_config_argv).cooked);
let browser = (argv.debug) ? 'Chrome' : 'PhantomJS';

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-phantomjs-launcher'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
      captureConsole: true
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage/angular-acme-bank'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true,
      thresholds: {
        emitWarning: false, // set to `true` to not fail the test command when thresholds are not met
        global: { // thresholds for all files
          statements: 100,
          lines: 100,
          branches: 100,
          functions: 100
        },
        each: { // thresholds per file
          statements: 100,
          lines: 100,
          branches: 100,
          functions: 100
        }
      }
    },
    angularCli: {
      environment: 'dev',
      codeCoverage: true
    },
    reporters: config.angularCli && config.angularCli.codeCoverage
                ? ['progress', 'coverage-istanbul']
                : ['progress', 'kjhtml'],
    browserNoActivityTimeout: 11000,
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: argv.debug ? true : false,
    /*
     * start these browsers
     * available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
     */
    browsers: [
      browser
    ],
    singleRun: argv.debug ? false : true,
    restartOnFileChange: true
  });
};
