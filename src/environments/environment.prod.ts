export const environment = {
  production: true,
  baseUrl: 'http://production-endpoint/api',
  endPoints: {
    accounts: 'accounts'
  }
};
